using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInstance : MonoBehaviour
{
    public static GameInstance Instance;
    
    // [Header("Game Variable")]
    // [SerializeField]
    
    [Header("Game Data")]
    [SerializeField] List<Item> gameItems = new List<Item>();
    public List<Item> GetGameItems() => gameItems;
    public List<ItemSlotData> playerItems;

    void Awake()
    {
        if (!Instance) Instance = this;
        else Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }
}
