using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PropsObject : MonoBehaviour
{
    [Header("Item")]
    [SerializeField] Item curItem;
    public Item GetItem() => curItem;

    [Header("Component")]
    [SerializeField] Transform visualTransform;
    [SerializeField] SpriteRenderer spriteRender;
    [SerializeField] BoxCollider2D boxCol;
    [SerializeField] CircleCollider2D cirCol;
    
    [Header("Props")]
    [SerializeField] PropsBase propsScript;

    [Header("Attributes")]
    bool isMovedPositionValid;
    [SerializeField] List<Collider2D> collidedProps;

    HashSet<Vector2Int> freeDeckCell;

    Vector3 curPos;

    void Awake() => SetItem(curItem);

    void Start() => curPos = transform.position;

    public void SetItem(Item item)
    {
        if (item) curItem = item;

        if (curItem && curItem.type == Item.Type.Spawnable)
        {
            spriteRender.sprite = curItem.itemSprite;

            visualTransform.localPosition = curItem.properties.propsPositionOffset;
            visualTransform.localEulerAngles = curItem.properties.propsRotationOffset;
            visualTransform.localScale = curItem.properties.propsScaleOffset;

            if (curItem.properties.isBoxCol)
            {
                boxCol.offset = curItem.properties.boxColOffset;
                boxCol.size = curItem.properties.boxColSize;
                cirCol.enabled = false;
            }
            else
            {
                cirCol.offset = curItem.properties.cirColOffset;
                cirCol.radius = curItem.properties.cirColRadius;
                boxCol.enabled = false;
            }

            switch (curItem.propsType)
            {
                case PropsType.Storage:
                    propsScript = GetComponent<PropsStorage>();
                    if (!propsScript) propsScript = gameObject.AddComponent<PropsStorage>();
                    break;
                case PropsType.Workbench:
                    propsScript = GetComponent<PropsWorkbench>();
                    if (!propsScript) propsScript = gameObject.AddComponent<PropsWorkbench>();
                    break;
                case PropsType.Wheel:
                    propsScript = GetComponent<PropsWheel>();
                    if (!propsScript) propsScript = gameObject.AddComponent<PropsWheel>();
                    break;
                default:
                    break;
            }
        }
    }

    public void Interact()
    {
        if (propsScript) propsScript.Interact();
    }

    public void StartMoveProps()
    {
        freeDeckCell = Deck.Instance.GetFreeDeckCell();
        freeDeckCell.Add((Vector2Int)Deck.Instance.GetBaseDeckTilemap().WorldToCell(curPos));

        if (curItem.properties.isBoxCol)
        {
            boxCol.isTrigger = true;
        }
        else
        {
            cirCol.isTrigger = true;
        }

        spriteRender.color = new Color(0, 1, 0, .5f);
    }

    public bool StopMoveProps()
    {
        if (isMovedPositionValid) curPos = transform.position;
        else transform.position = curPos;

        Targeter.Instance.UpdateTargeter();

        Deck.Instance.UpdateTiling();

        if (curItem.properties.isBoxCol)
        {
            boxCol.isTrigger = false;
        }
        else
        {
            cirCol.isTrigger = false;
        }
        spriteRender.color = Color.white;

        return isMovedPositionValid;
    }

    public void MovePropsToPos(Vector3 pos)
    {
        transform.position = pos;

        if (freeDeckCell.Contains((Vector2Int)Deck.Instance.GetBaseDeckTilemap().WorldToCell(pos)))
        {
            isMovedPositionValid = true;
            spriteRender.color = new Color(0, 1, 0, .5f);
        }
        else
        {
            isMovedPositionValid = false;
            spriteRender.color = new Color(1, 0, 0, .5f);
        }
    }
}
