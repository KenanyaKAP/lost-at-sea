using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PropsType
{
    None,
    Storage,
    Workbench,
    Wheel,
}
public class PropsBase : MonoBehaviour
{
    PropsObject parentObject;

    void Start() => parentObject = GetComponent<PropsObject>();

    public virtual void Interact()
    {

    }

    public void MoveProps()
    {
        PlayerMoveProps.Instance.MoveProps(this, parentObject);
    }

    // public virtual void DestroyProps()
    // {

    // }
}