using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPopUp : MonoBehaviour
{
    [Header("Component")]
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] RectTransform canvas;
    [SerializeField] TMPro.TextMeshProUGUI text;

    [Header("Attributes")]
    [SerializeField] Item curItem;
    [SerializeField] int curItemCount = 1;

    void Awake() => StartPopUp(curItem, curItemCount);

    public void StartPopUp(Item item, int itemCount)
    {
        if (item == null || itemCount == 0)
        {
            Destroy(gameObject);
            return;
        }
        if (item.type != Item.Type.Material && item.type != Item.Type.Tool)
        {
            Destroy(gameObject);
            return;
        }

        curItem = item;

        spriteRenderer.sprite = curItem.itemSpriteOutline;
        transform.localScale = item.materialScale;
        canvas.localScale = new Vector3(.019f/item.materialScale.x, .019f/item.materialScale.y, .019f/item.materialScale.z);

        text.text = (itemCount > 0) ? "+" + itemCount.ToString() : "-" + Mathf.Abs(itemCount).ToString();

        LeanTween.move(gameObject, transform.position + Vector3.up, .6f).setEaseOutQuad().setOnComplete(() => Destroy(gameObject));
        LeanTween.value(gameObject, (value) => {
            spriteRenderer.color = new Color(1, 1, 1, value);
            text.color = new Color(1, 1, 1, value);
        }, 1, 0, .6f);
    }
}
