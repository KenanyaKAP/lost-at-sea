using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropsWorkbench : PropsBase
{
    [Header("Components")]
    [SerializeField] GameObject workbenchUI;
    [SerializeField] GameObject workbenchUIBackButton;
    [SerializeField] GameObject propsObjectPrefabs;
    [SerializeField] Transform propsParent;

    [Header("Attributes")]
    [SerializeField] PropsBase propsToMove;
    [SerializeField] PropsObject propsObjectToMove;
    
    [Header("Extra Components")]
    [SerializeField] GameObject[] needToSwitchActive;
    [SerializeField] GameObject[] uiNeedToTurnOff;

    bool isMovingObject;

    public override void Interact()
    {
        base.Interact();

        workbenchUI.SetActive(true);
        workbenchUIBackButton.SetActive(true);
    }

    public void BuildProps(int itemId) // Called By Build Button
    {
        // Instantiating props object
        // Move that object
        // Change player state to buildProps

        // Wheter player have enough material
        foreach (ItemSlotData data in GameInstance.Instance.GetGameItems().Find((x) => x.itemId == itemId).recipe)
        {
            if (PlayerInventory.Instance.GetSpecifiedItemCount((int)data.item.itemId) < data.count) return;
        }

        foreach (GameObject item in uiNeedToTurnOff) item.SetActive(false);

        PropsObject temp = Instantiate(propsObjectPrefabs, Vector3.zero, Quaternion.identity, propsParent).GetComponent<PropsObject>();
        temp.SetItem(GameInstance.Instance.GetGameItems().Find((x) => x.itemId == itemId));
        if (temp.GetComponent<PropsBase>() == null) Debug.Log("NULL WANTING");

        MoveProps(temp.GetComponent<PropsBase>(), temp);
    }

    void Update()
    {
        if (isMovingObject)
        {
            MovePropsObjectToMove(Locator.Instance.transform.position.Round());
        }
    }

    public void MoveProps(PropsBase props, PropsObject propsObject)
    {
        propsToMove = props;
        propsObjectToMove = propsObject;

        // Change player state
        // Temporary disable the props object
        // Make props object to green or red
        // Move props dynamicly => match closes tile with the locator

        isMovingObject = true;
        Player.Instance.ChangeState(Player.State.BuildProps);
        propsObjectToMove.StartMoveProps();

        foreach (GameObject temp in needToSwitchActive)
        {
            temp.SetActive(!temp.activeSelf);
        }
    }

    void MovePropsObjectToMove(Vector3 pos)
    {
        if (propsObjectToMove.transform.position == pos) return;

        propsObjectToMove.MovePropsToPos(pos);
    }

    public void OnClick()
    {
        isMovingObject = false;
        Player.Instance.ChangeState(Player.State.Default);
        if (propsObjectToMove.StopMoveProps())
        {
            foreach (ItemSlotData data in GameInstance.Instance.GetGameItems().Find((x) => x.itemId == propsObjectToMove.GetItem().itemId).recipe)
            {
                PlayerInventory.Instance.RemoveItem((int)data.item.itemId, data.count);
            }
        }
        else
        {
            Destroy(propsObjectToMove.gameObject);
        }

        foreach (GameObject temp in needToSwitchActive)
        {
            temp.SetActive(!temp.activeSelf);
        }
    }
}
