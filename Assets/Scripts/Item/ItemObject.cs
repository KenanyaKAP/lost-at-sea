using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class ItemObject : MonoBehaviour
{
    [Header("Component")]
    [SerializeField] RectTransform canvas;
    [SerializeField] TMPro.TextMeshProUGUI text;

    [Header("Properties")]
    [SerializeField] ItemSlotData item;
    public ItemSlotData GetItem() => item;
    public bool freshItem {get; set;}
    // [SerializeField] Item item;
    // public Item GetItem() => item;
    // [SerializeField] int count = 1;
    // public int GetCount() => count;

    SpriteRenderer render;

    void Awake()
    {
        render = GetComponent<SpriteRenderer>();
    }

    // void Start() => SetItem(item, count);
    void Start() => SetItem(item);

    public void SetItem(ItemSlotData newItem)
    {
        if (newItem == null)
        {
            Destroy(gameObject);
            return;
        }
        if (!newItem.item || newItem.count == 0)
        {
            Destroy(gameObject);
            return;
        }
        if (newItem.item.type != Item.Type.Material && newItem.item.type != Item.Type.Tool)
        {
            Destroy(gameObject);
            return;
        }

        item = newItem;

        if (item.count > 1) text.SetText(item.count.ToString());
        
        render.sprite = item.item.itemSpriteOutline;
        transform.localScale = item.item.materialScale;
        canvas.localScale = new Vector3(.019f/item.item.materialScale.x, .019f/item.item.materialScale.y, .019f/item.item.materialScale.z);
    }
    
    // public void SetItem(Item newItem, int newCount)
    // {
    //     if (!newItem) return;
    //     if (newItem.type != Item.Type.Material && newItem.type != Item.Type.Tool) return;

    //     item = newItem;
    //     count = newCount;

    //     if (count > 1) text.SetText(count.ToString());
        
    //     render.sprite = item.itemSpriteOutline;
    //     transform.localScale = item.scale;
    //     canvas.localScale = new Vector3(.019f/item.scale.x, .019f/item.scale.y, .019f/item.scale.z);
    // }
}
