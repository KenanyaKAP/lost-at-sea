using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropsStorage : PropsBase
{
    [Header("Properties")]
    [SerializeField] int storageSize = 10;
    [SerializeField] int storageStack = 20;
    public int GetStorageSize() => storageSize;

    [Header("Items")]
    [SerializeField] List<ItemSlotData> items = new List<ItemSlotData>();
    [SerializeField] List<ItemSlotData> itemsBySlot = new List<ItemSlotData>();
    public List<ItemSlotData> GetItemsBySlot() => itemsBySlot;
    public override void Interact()
    {
        base.Interact();

        StorageUI.Instance.OpenStorage(this);
    }

    void Awake()
    {
        items.Sort(Utilities.ItemSlotCompare);
        ReArrangeItemBySlot();
    }

    public bool CanAddItemWithReturn(ItemSlotData itemToAdd)
    {
        int capacity = (storageSize - itemsBySlot.Count) * storageStack;
        foreach (ItemSlotData item in itemsBySlot)
        {
            if (item.item == itemToAdd.item && item.count < storageStack)
            {
                capacity += storageStack - item.count;
            }
        }

        if (capacity <= 0) return false;

        if (itemToAdd.count > capacity)
        {
            itemToAdd.count = capacity;
        }

        ItemSlotData temp = items.Find((v) => v.item == itemToAdd.item);
        if (temp != null)
        {
            temp.count += itemToAdd.count;
        }
        else
        {
            items.Add(new ItemSlotData(itemToAdd.item, itemToAdd.count));

            items.Sort(Utilities.ItemSlotCompare);
        }

        ReArrangeItemBySlot();

        return true;
    }

    public void RemoveItem(ItemSlotData itemToRemove)
    {
        ItemSlotData temp = items.Find((v) => v.item == itemToRemove.item);
        if (temp != null)
        {
            if (itemToRemove.count > temp.count) Debug.Log("WARNING REMOVING MORE ITEM THAN IT SHOULD BE!");
            
            temp.count -= itemToRemove.count;
            if (temp.count <= 0)
            {
                items.Remove(temp);
            }
        }

        ReArrangeItemBySlot();
    }

    public void ReArrangeItemBySlot()
    {
        itemsBySlot.Clear();

        foreach (ItemSlotData item in items)
        {
            int itemCount = item.count;
            while (itemCount > 0)
            {
                if (itemCount >= storageStack)
                {
                    itemsBySlot.Add(new ItemSlotData(item.item, storageStack));
                    itemCount -= storageStack;
                }
                else
                {
                    itemsBySlot.Add(new ItemSlotData(item.item, itemCount));
                    itemCount = 0;
                }
            }
        }
    }

    public void DestroyProps()
    {
        foreach (ItemSlotData slot in itemsBySlot)
        {
            PlayerInventory.Instance.SpawnItem(slot, transform.position);
        }
        foreach (ItemSlotData slot in GetComponent<PropsObject>().GetItem().recipe)
        {
            PlayerInventory.Instance.SpawnItem(slot, transform.position);
        }

        Destroy(gameObject);
    }
}
