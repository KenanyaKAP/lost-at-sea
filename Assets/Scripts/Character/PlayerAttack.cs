using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerAttack : MonoBehaviour
{
    public static PlayerAttack Instance;

    [Header("Component")]
    [SerializeField] PlayerMovement playerMovement;
    [SerializeField] Slider healtBar;
    [SerializeField] PlayerAttackRadius atkRadius;
    [SerializeField] GameObject arrowPrefabs;
    [SerializeField] Animator animator;
    
    [Header("Attributes")]
    [SerializeField] float hp = 1f;
    [SerializeField] bool isDead;
    [SerializeField] float damageAmount = .3f;
    [SerializeField] float coolDownTime = 1f;

    float atkTime = 0;
    
    void Awake()
    {
        if (!Instance) Instance = this;
        else Destroy(this);
    }

    void Update()
    {
        atkTime = Mathf.Min(atkTime + Time.deltaTime, coolDownTime + .1f);

        if (playerMovement.GetMovementMagnitude() < .1f)
        {
            if (atkRadius.enemies.Count > 0)
            {
                if (atkTime > coolDownTime)
                {
                    AttackClosesEnemy();
                    atkTime -= coolDownTime;
                }
            }
        }
    }

    public void DamagePlayer(float damage, Vector3 dir)
    {
        if (isDead) return;

        hp -= damage;
        healtBar.value = hp;

        LeanTween.cancel(gameObject);
        LeanTween.move(gameObject, transform.position + dir * .5f, .3f).setEaseOutQuad();

        if (hp <= 0) Dead();
    }

    void Dead()
    {
        isDead = true;

        SceneManager.LoadScene("MainDeck");
    }

    void AttackClosesEnemy()
    {
        Vector3 pos = Vector3.zero;
        float dis = float.MaxValue;

        animator.SetBool("isAttacking", true);
        LeanTween.value(gameObject, 0, 1, .2f).setOnComplete(() => animator.SetBool("isAttacking", false));

        foreach (Enemy enemy in atkRadius.enemies)
        {
            if (Vector3.Distance(enemy.transform.position, transform.position) < dis)
            {
                dis = Vector3.Distance(enemy.transform.position, transform.position);
                pos = enemy.transform.position;
            }
        }

        Arrow temp = Instantiate(arrowPrefabs, transform.position, Quaternion.identity).GetComponent<Arrow>();
        temp.SetArrow(pos - transform.position, damageAmount);
        
        playerMovement.RotatePlayer((pos - transform.position).x * 1000);
    }
}
