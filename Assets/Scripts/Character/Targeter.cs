using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targeter : MonoBehaviour
{
    public static Targeter Instance;

    [Header("Properties")]
    [SerializeField] bool targetInField;

    [Header("Component")]
    [SerializeField] Transform visual;
    [SerializeField] List<Vector3> propsPosition = new List<Vector3>();
    
    [Header("Reference")]
    [SerializeField] Joystick joystick;

    Vector3 targetPos;

    void Awake()
    {
        if (!Instance) Instance = this;
        else Destroy(this);
    }

    void Start()
    {
        LeanTween.moveLocalY(visual.gameObject, .75f, .4f).setEaseInQuad().setLoopPingPong();
        joystick.onJoystickDrag += SeekingTarget;
    }

    void SeekingTarget(Vector2 dir)
    {
        if (CheckCloseObject())
        {
            LeanTween.cancel(gameObject);
            LeanTween.move(gameObject, targetPos, .2f).setEaseInOutQuad();
        }

        UpdateTargetInField();
    }

    void UpdateTargetInField()
    {
        if (targetInField && propsPosition.Count == 0)
        {
            targetInField = false;
            return;
        }

        if (!targetInField && propsPosition.Contains(targetPos))
        {
            targetInField = true;
        }
    }

    bool CheckCloseObject()
    {
        if (propsPosition.Count == 0) return false;
        
        float temp = float.MaxValue;
        Vector3 tempGoalPos = Vector3.zero;
        foreach (Vector3 propsPos in propsPosition)
        {
            if (Vector3.Distance(propsPos, Locator.Instance.transform.position) < temp)
            {
                temp = Vector3.Distance(propsPos, Locator.Instance.transform.position);
                tempGoalPos = propsPos;
            }
        }

        if (tempGoalPos != targetPos)
        {
            targetPos = tempGoalPos;
            return true;
        }
        return false;
    }

    public PropsObject GetTargetPropsObject()
    {
        if (!targetInField) return null;

        return Locator.Instance.GetPropsInside()[propsPosition.IndexOf(targetPos)];
    }

    public void UpdateTargeter()
    {
        List<PropsObject> temp = Locator.Instance.GetPropsInside();
        propsPosition.Clear();

        foreach (PropsObject props in temp)
        {
            propsPosition.Add(props.transform.position);
        }
    }
}
