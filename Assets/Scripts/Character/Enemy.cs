using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] Slider healthBar;
    [SerializeField] GameObject itemObjectPrefabs;

    [Header("Movement Configuration")]
    [SerializeField] float moveSpeed = 3.5f;
    [SerializeField] float moveTurnSpeed = 20f;
    [SerializeField] float closeDistance = 2f;

    [Header("Animation")]
    [SerializeField] Transform visual;
    [SerializeField] Animator animator;
    
    [Header("Attack Configuration")]
    [SerializeField] float damage = .101f;
    [SerializeField] float coolDownTime = .6f;
    Rigidbody2D rb;
    Vector2 movement = new Vector2(0, 0);
    Vector2 externalMoveDirection;
    bool chasePlayer;
    GameObject player;
    Vector3 chaseDir;

    float atkTime = 1;

    Vector3 lastDir;

    float hp = 1;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        movement = Vector2.Lerp(movement, chaseDir, moveTurnSpeed * Time.deltaTime);

        if (chasePlayer)
        {
            if (Vector3.Distance(player.transform.position, transform.position) > closeDistance)
            {
                chaseDir = (player.transform.position - transform.position).normalized;
                lastDir = chaseDir;
            }
            else
            {
                chaseDir = Vector3.zero;
                atkTime += Time.deltaTime;

                if (atkTime > coolDownTime)
                {
                    atkTime -= coolDownTime;
                    PlayerAttack.Instance.DamagePlayer(damage, lastDir);
                }
            }
        }
        else
        {
            
        }

        if (chaseDir.magnitude != 0) animator.SetBool("isMoving", true);
        else animator.SetBool("isMoving", false);

        if (Mathf.Sign(movement.x) == 1 && visual.localScale.x != 1) visual.localScale = Vector3.one;
        else if (Mathf.Sign(movement.x) == -1 && visual.localScale.x != -1) visual.localScale = new Vector3(-1, 1, 1);
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            chasePlayer = true;
            player = Player.Instance.gameObject;
        }
    }
    
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player")) chasePlayer = false;
    }

    public void Damage(float damage, Vector3 dir)
    {
        if (hp <= 0) return;

        LeanTween.move(gameObject, transform.position + dir * .3f, .3f).setEaseOutQuad();

        hp -= damage;
        healthBar.value = hp;

        if (hp <= 0) Dead();
    }

    public void Dead()
    {
        SpawnItem(0, Random.Range(1, 3));

        Destroy(gameObject);
    }

    void SpawnItem(int itemId, int count)
    {
        ItemObject temp = Instantiate(itemObjectPrefabs, transform.position, Quaternion.identity).GetComponent<ItemObject>();
        temp.transform.localScale = Vector3.one * .3f;
        ItemSlotData toSpawn = new ItemSlotData(GameInstance.Instance.GetGameItems().Find((x) => x.itemId == itemId), count);
        LeanTween.scale(temp.gameObject, toSpawn.item.materialScale, .2f).setEaseInQuad();
        LeanTween.move(temp.gameObject, temp.transform.position + Vector3.up * .5f, .15f).setEaseOutQuad().setLoopPingPong(1);
        temp.freshItem = false;
        temp.SetItem(toSpawn);
    }
}
