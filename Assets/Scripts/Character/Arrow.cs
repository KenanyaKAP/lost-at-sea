using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    [SerializeField] float moveSpeed = 1f;

    [SerializeField] Vector3 moveDir;
    float damageAmount;
    Rigidbody2D rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("EnemyCollider"))
        {
            other.GetComponentInParent<Enemy>().Damage(damageAmount, moveDir);
            Destroy(gameObject);
        }

        if (other.CompareTag("Wall")) Destroy(gameObject);
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + (Vector2)moveDir.normalized * moveSpeed * Time.fixedDeltaTime);
    }

    public void SetArrow(Vector3 dir, float damage)
    {
        moveDir = dir;
        damageAmount = damage;

        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
