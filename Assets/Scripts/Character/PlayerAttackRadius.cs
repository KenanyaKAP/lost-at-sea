using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackRadius : MonoBehaviour
{
    public List<Enemy> enemies;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("EnemyCollider")) enemies.Add(other.GetComponentInParent<Enemy>());
    }
    
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("EnemyCollider")) enemies.Remove(other.GetComponentInParent<Enemy>());
    }
}
