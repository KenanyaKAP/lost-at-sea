using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    [Header("Component")]
    [SerializeField] Joystick joystick;
    public Joystick GetJoystick() => joystick;

    [Header("Movement Configuration")]
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] float moveTurnSpeed = 20f;

    [Header("Animation")]
    [SerializeField] Transform visual;
    [SerializeField] Animator animator;

    Rigidbody2D rb;
    Vector2 movement;
    Vector2 externalMoveDirection;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        movement = Vector2.Lerp(movement, joystick.Direction.normalized + externalMoveDirection.normalized, moveTurnSpeed * Time.deltaTime);

        if (movement.magnitude > .1f) RotatePlayer(movement.x);

        if (movement.magnitude < .1f) animator.SetBool("isMoving", false);
        else animator.SetBool("isMoving", true);
    }

    public float GetMovementMagnitude() => movement.magnitude;

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
    }

    public void SetVelocityExternaly(Vector2 direction)
    {
        externalMoveDirection = direction;
    }

    public void RotatePlayer(float signDir)
    {
        if (Mathf.Sign(signDir) == 1 && Mathf.Sign(visual.localScale.x) != 1) visual.localScale = new Vector3(-visual.localScale.x, visual.localScale.y, visual.localScale.z);
        else if (Mathf.Sign(signDir) == -1 && Mathf.Sign(visual.localScale.x) != -1) visual.localScale = new Vector3(-visual.localScale.x, visual.localScale.y, visual.localScale.z);
    }
}