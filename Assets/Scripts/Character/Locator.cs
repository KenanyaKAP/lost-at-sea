using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Locator : MonoBehaviour
{
    public static Locator Instance;

    [Header("Properties")]
    [SerializeField] float distance = 1f;

    [Header("Joystick")]
    [SerializeField] Joystick joystick;

    [Header("Interactable Item")]
    [SerializeField] List<PropsObject> propsInside;
    public List<PropsObject> GetPropsInside() => propsInside;

    void Awake()
    {
        if (!Instance) Instance = this;
        else Destroy(this);
    }

    void Start()
    {
        joystick.onJoystickDrag += MoveLocator;
    }

    void MoveLocator(Vector2 dir)
    {
        transform.localPosition = dir.normalized * distance;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Props"))
        {
            propsInside.Add(other.GetComponentInParent<PropsObject>());
            Targeter.Instance.UpdateTargeter();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Props"))
        {
            propsInside.Remove(other.GetComponentInParent<PropsObject>());
            Targeter.Instance.UpdateTargeter();
        }
    }
}
