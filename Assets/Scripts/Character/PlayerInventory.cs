using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    public static PlayerInventory Instance;
    
    [Header("Properties")]
    [SerializeField] int maxSlot = 15;
    public int GetMaxSlot() => maxSlot;
    [SerializeField] int maxStack = 20;
    public int GetMaxStack() => maxStack;

    [Header("Components")]
    [SerializeField] GameObject itemObjectPrefabs;
    [SerializeField] Transform locator;

    [Header("Items")]
    [SerializeField] List<ItemSlotData> items;
    [SerializeField] List<ItemSlotData> itemsBySlot;
    public List<ItemSlotData> GetItemsBySlot() => itemsBySlot;

    void Awake()
    {
        if (!Instance) Instance = this;
        else Destroy(this);

        items.Sort(Utilities.ItemSlotCompare);
        ReArrangeItemBySlot();
    }

    void Start()
    {
        items = GameInstance.Instance.playerItems;
        items.Sort(Utilities.ItemSlotCompare);
        ReArrangeItemBySlot();
    }

    public bool CanAddItemWithReturn(ItemSlotData itemToAdd)
    {
        int capacity = (maxSlot - itemsBySlot.Count) * maxStack;
        foreach (ItemSlotData item in itemsBySlot)
        {
            if (item.item == itemToAdd.item && item.count < maxStack)
            {
                capacity += maxStack - item.count;
            }
        }

        if (capacity <= 0) return false;

        if (itemToAdd.count > capacity)
        {
            itemToAdd.count = capacity;
        }

        ItemSlotData temp = items.Find((v) => v.item == itemToAdd.item);
        if (temp != null)
        {
            temp.count += itemToAdd.count;
        }
        else
        {
            items.Add(new ItemSlotData(itemToAdd.item, itemToAdd.count));

            items.Sort(Utilities.ItemSlotCompare);
        }

        GameInstance.Instance.playerItems = items;
        
        ReArrangeItemBySlot();

        return true;
    }

    public int CanAddItemWithReturn(int itemId, int count)
    {
        int returnCount = count;
        int capacity = (maxSlot - itemsBySlot.Count) * maxStack;
        foreach (ItemSlotData item in itemsBySlot)
        {
            if (item.item.itemId == itemId && item.count < maxStack)
            {
                capacity += maxStack - item.count;
            }
        }

        if (capacity <= 0) return returnCount;

        if (count > capacity)
        {
            returnCount = count - capacity;
            count = capacity;
        }
        else returnCount = 0;

        ItemSlotData temp = items.Find((v) => v.item.itemId == itemId);
        if (temp != null)
        {
            temp.count += count;
        }
        else
        {
            items.Add(new ItemSlotData(GameInstance.Instance.GetGameItems().Find((x) => x.itemId == itemId), count));

            items.Sort(Utilities.ItemSlotCompare);
        }

        ReArrangeItemBySlot();

        return returnCount;
    }

    public void RemoveItem(ItemSlotData itemToRemove)
    {
        ItemSlotData temp = items.Find((v) => v.item == itemToRemove.item);
        if (temp != null)
        {
            if (itemToRemove.count > temp.count) Debug.Log("WARNING REMOVING MORE ITEM THAN IT SHOULD BE!");
            
            temp.count -= itemToRemove.count;
            if (temp.count <= 0)
            {
                items.Remove(temp);
            }
        }

        ReArrangeItemBySlot();
    }

    public void RemoveItem(int itemId, int count)
    {
        ItemSlotData temp = items.Find((v) => v.item.itemId == itemId);
        if (temp != null)
        {
            if (count > temp.count) Debug.Log("WARNING REMOVING MORE ITEM THAN IT SHOULD BE!");
            
            temp.count -= count;
            if (temp.count <= 0)
            {
                items.Remove(temp);
            }
        }

        ReArrangeItemBySlot();
    }

    public void ReArrangeItemBySlot()
    {
        itemsBySlot.Clear();

        foreach (ItemSlotData item in items)
        {
            int itemCount = item.count;
            while (itemCount > 0)
            {
                if (itemCount >= maxStack)
                {
                    itemsBySlot.Add(new ItemSlotData(item.item, maxStack));
                    itemCount -= maxStack;
                }
                else
                {
                    itemsBySlot.Add(new ItemSlotData(item.item, itemCount));
                    itemCount = 0;
                }
            }
        }
    }

    public void SpawnItem(ItemSlotData itemToSpawn)
    {
        ItemObject temp = Instantiate(itemObjectPrefabs, transform.position, Quaternion.identity).GetComponent<ItemObject>();
        temp.transform.localScale = Vector3.one * .3f;
        LeanTween.move(temp.gameObject, transform.position + locator.localPosition + (Vector3)Random.insideUnitCircle * .5f, .2f).setEaseInQuad();
        LeanTween.scale(temp.gameObject, itemToSpawn.item.materialScale, .2f).setEaseInQuad();
        temp.freshItem = true;
        temp.SetItem(itemToSpawn);
    }
    
    public void SpawnItem(int itemId, int count)
    {
        ItemObject temp = Instantiate(itemObjectPrefabs, transform.position, Quaternion.identity).GetComponent<ItemObject>();
        temp.transform.localScale = Vector3.one * .3f;
        ItemSlotData toSpawn = new ItemSlotData(GameInstance.Instance.GetGameItems().Find((x) => x.itemId == itemId), count);
        LeanTween.move(temp.gameObject, transform.position + locator.localPosition + (Vector3)Random.insideUnitCircle * .5f, .2f).setEaseInQuad();
        LeanTween.scale(temp.gameObject, toSpawn.item.materialScale, .2f).setEaseInQuad();
        temp.freshItem = true;
        temp.SetItem(toSpawn);
    }

    public void SpawnItem(ItemSlotData itemToSpawn, Vector3 position)
    {
        ItemObject temp = Instantiate(itemObjectPrefabs, transform.position, Quaternion.identity).GetComponent<ItemObject>();
        temp.transform.localScale = Vector3.one * .3f;
        LeanTween.move(temp.gameObject, position, .2f).setEaseInQuad();
        LeanTween.scale(temp.gameObject, itemToSpawn.item.materialScale, .2f).setEaseInQuad();
        temp.freshItem = true;
        temp.SetItem(itemToSpawn);
    }
    
    // public void ReArrangeItemBySlot()
    // {
    //     items.Sort(Utilities.ItemCompare);
    //     itemsBySlot.Clear();

    //     Item oldItem = null;
    //     int oldItemCount = 0;

    //     foreach (Item theItem in items)
    //     {
    //         if (oldItem != theItem || oldItemCount >= maxStack)
    //         {
    //             // Store prev Item
    //             if (oldItem) itemsBySlot.Add(new InventorySlotData(oldItem, oldItemCount));

    //             // Create new Item for loop
    //             oldItem = theItem;
    //             oldItemCount = 1;
    //         }
    //         else
    //         {
    //             oldItemCount += 1;
    //         }
    //     }
    //     if (oldItem) itemsBySlot.Add(new InventorySlotData(oldItem, oldItemCount));
    // }

    public int GetSpecifiedItemCount(int itemId)
    {
        ItemSlotData temp = items.Find((x) => x.item.itemId == itemId);
        if (temp != null) return temp.count;
        return 0;
    }
}
