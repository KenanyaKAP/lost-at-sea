using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveProps : MonoBehaviour
{
    public static PlayerMoveProps Instance;

    [Header("Attributes")]
    [SerializeField] PropsBase propsToMove;
    [SerializeField] PropsObject propsObjectToMove;
    
    [Header("Extra Components")]
    [SerializeField] GameObject[] needToSwitchActive;

    bool isMovingObject = false;

    void Awake()
    {
        if (!Instance) Instance = this;
        else Destroy(this);
    }

    void Update()
    {
        if (isMovingObject)
        {
            MovePropsObjectToMove(Locator.Instance.transform.position.Round());
        }
    }

    public void MoveProps(PropsBase props, PropsObject propsObject)
    {
        propsToMove = props;
        propsObjectToMove = propsObject;

        // Change player state
        // Temporary disable the props object
        // Make props object to green or red
        // Move props dynamicly => match closes tile with the locator

        isMovingObject = true;
        Player.Instance.ChangeState(Player.State.MoveProps);
        propsObjectToMove.StartMoveProps();

        foreach (GameObject temp in needToSwitchActive)
        {
            temp.SetActive(!temp.activeSelf);
        }
    }

    void MovePropsObjectToMove(Vector3 pos)
    {
        if (propsObjectToMove.transform.position == pos) return;

        propsObjectToMove.MovePropsToPos(pos);
    }

    public void OnClick()
    {
        isMovingObject = false;
        Player.Instance.ChangeState(Player.State.Default);
        propsObjectToMove.StopMoveProps();

        foreach (GameObject temp in needToSwitchActive)
        {
            temp.SetActive(!temp.activeSelf);
        }
    }
}
