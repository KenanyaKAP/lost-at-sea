using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMagnet : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Item"))
        {
            if (col.GetComponent<ItemObject>().freshItem) col.GetComponent<ItemObject>().freshItem = false;
            else
            {
                ItemSlotData temp = new ItemSlotData(col.GetComponent<ItemObject>().GetItem());
                if (PlayerInventory.Instance.CanAddItemWithReturn(temp))
                {
                    if (temp.count != col.GetComponent<ItemObject>().GetItem().count)
                    {
                        temp.count = col.GetComponent<ItemObject>().GetItem().count - temp.count;
                        PlayerInventory.Instance.SpawnItem(temp);
                    }
                    Collect(col);
                }
            }
        }
    }

    void Collect(Collider2D item)
    {
        LeanTween.move(item.gameObject, transform.position, .2f).setEaseInQuad();
        LeanTween.scale(item.gameObject, Vector3.one * .3f, .2f).setEaseInQuad().setOnComplete(() => Destroy(item.gameObject));
    }
}
