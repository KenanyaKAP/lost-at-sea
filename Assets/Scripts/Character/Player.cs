using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
public class Player : MonoBehaviour
{
    public enum State
    {
        Default,
        BuildProps,
        MoveProps,
        EditDeck,
        Batle,
    }

    public static Player Instance;

    void Awake()
    {
        if (!Player.Instance) Player.Instance = this;
        else Destroy(this);
    }

    [Header("Component")]
    [SerializeField] Transform playerPivot;
    [SerializeField] PlayerMovement playerMovement;
    
    [Header("External Component")]
    [SerializeField] PropsWorkbench propsWorkbench;

    [Header("Attributes")]
    [SerializeField] State state = State.Default;
    public State GetState() => state;
    [SerializeField] State prevState = State.Default;
    
    [Header("Properties")]
    [SerializeField] float stopMoveDistance = .6f;

    bool isGoingToTarget;
    PropsObject propsTarget;

    void Start()
    {
        JoystickTap.onJoystickTap += PlayerTap;
    }

    void Update()
    {
        if (isGoingToTarget)
        {
            playerMovement.SetVelocityExternaly(propsTarget.transform.position - playerPivot.position);
            
            if (playerMovement.GetJoystick().Direction.magnitude > 0)
            {
                playerMovement.SetVelocityExternaly(Vector2.zero);
                isGoingToTarget = false;
            }

            if (Vector3.Distance(playerPivot.position, propsTarget.transform.position) < stopMoveDistance)
            {
                playerMovement.SetVelocityExternaly(Vector2.zero);
                propsTarget.Interact();
                isGoingToTarget = false;
            }
        }
    }

    void PlayerTap()
    {
        switch (state)
        {
            case State.Default:
                SetTarget(Targeter.Instance.GetTargetPropsObject());
                break;
            case State.BuildProps:
                propsWorkbench.OnClick();
                break;
            case State.MoveProps:
                PlayerMoveProps.Instance.OnClick();
                break;
            case State.EditDeck:
                Deck.Instance.OnClick();
                break;
        }
    }

    void SetTarget(PropsObject target)
    {
        if (!target) return;
        propsTarget = target;
        isGoingToTarget = true;
    }

    public void ChangeState(State newState)
    {
        if (newState == state) return;

        prevState = state;
        state = newState;

        // Switching between states
        if (prevState == State.Default)
        {
            if (state == State.MoveProps)
            {
                Targeter.Instance.gameObject.SetActive(false);
            }
            else if (state == State.EditDeck)
            {
                Targeter.Instance.gameObject.SetActive(false);
            }
            else if (state == State.BuildProps)
            {
                Targeter.Instance.gameObject.SetActive(false);
            }
        }
        else if (prevState == State.MoveProps)
        {
            if (state == State.Default)
            {
                Targeter.Instance.gameObject.SetActive(true);
            }
        }
        else if (prevState == State.EditDeck)
        {
            if (state == State.Default)
            {
                Targeter.Instance.gameObject.SetActive(true);
            }
        }
        else if (prevState == State.BuildProps)
        {
            if (state == State.Default)
            {
                Targeter.Instance.gameObject.SetActive(true);
            }
        }
    }
}
