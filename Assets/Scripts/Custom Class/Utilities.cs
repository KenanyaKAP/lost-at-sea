using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public static class Utilities
{
    public static HashSet<Vector2Int> GetNeighborCell(Vector2Int pos)
    {
        HashSet<Vector2Int> temp = new HashSet<Vector2Int>();
        temp.Add(new Vector2Int(pos.x - 1, pos.y - 1));
        temp.Add(new Vector2Int(pos.x - 1, pos.y));
        temp.Add(new Vector2Int(pos.x - 1, pos.y + 1));
        temp.Add(new Vector2Int(pos.x, pos.y - 1));
        temp.Add(new Vector2Int(pos.x, pos.y + 1));
        temp.Add(new Vector2Int(pos.x + 1, pos.y - 1));
        temp.Add(new Vector2Int(pos.x + 1, pos.y));
        temp.Add(new Vector2Int(pos.x + 1, pos.y + 1));
        return temp;
    }
    
    public static HashSet<Vector2Int> GetNeighborCell(Vector2Int pos, HashSet<Vector2Int> tilePos)
    {
        HashSet<Vector2Int> temp = new HashSet<Vector2Int>();
        foreach (Vector2Int tile in GetNeighborCell(pos))
        {
            if (tilePos.Contains(tile)) temp.Add(tile);
        }
        return temp;
    }
    
    public static HashSet<Vector2Int> GetStraightNeighborCell(Vector2Int pos)
    {
        HashSet<Vector2Int> temp = new HashSet<Vector2Int>();
        temp.Add(new Vector2Int(pos.x - 1, pos.y));
        temp.Add(new Vector2Int(pos.x, pos.y - 1));
        temp.Add(new Vector2Int(pos.x, pos.y + 1));
        temp.Add(new Vector2Int(pos.x + 1, pos.y));
        return temp;
    }
    
    public static HashSet<Vector2Int> GetStraightNeighborCell(Vector2Int pos, HashSet<Vector2Int> tilePos)
    {
        HashSet<Vector2Int> temp = new HashSet<Vector2Int>();
        foreach (Vector2Int tile in GetStraightNeighborCell(pos))
        {
            if (tilePos.Contains(tile)) temp.Add(tile);
        }
        return temp;
    }

    public static HashSet<Vector2Int> GetTilePosOfType(Tilemap tilemap, TileBase tile)
    {
        HashSet<Vector2Int> temp = new HashSet<Vector2Int>();
        BoundsInt bound = tilemap.cellBounds;
        for (int x = bound.xMin; x < bound.xMax; x++)
        {
            for (int y = bound.yMin; y < bound.yMax; y++)
            {
                TileBase tileTemp = tilemap.GetTile(new Vector3Int(x, y));
                if (tileTemp == tile)
                {
                    temp.Add(new Vector2Int(x, y));
                }
            }
        }
        return temp;
    }

    public static HashSet<Vector2Int> GetOuterTilePosOfType(Tilemap tilemap, TileBase tile, HashSet<Vector2Int> tilePos)
    {
        HashSet<Vector2Int> temp = new HashSet<Vector2Int>();
        foreach (Vector2Int tempTilePos in tilePos)
        {
            foreach (Vector2Int pos in Utilities.GetNeighborCell(tempTilePos))
            {
                if (tilemap.GetTile((Vector3Int)pos) != tile)
                {
                    temp.Add(pos);
                }
            }
        }
        return temp;
    }

    public static HashSet<Vector2Int> GetOuterStraightTilePosOfType(Tilemap tilemap, TileBase tile, HashSet<Vector2Int> tilePos)
    {
        HashSet<Vector2Int> temp = new HashSet<Vector2Int>();
        foreach (Vector2Int tempTilePos in tilePos)
        {
            foreach (Vector2Int pos in Utilities.GetStraightNeighborCell(tempTilePos))
            {
                if (tilemap.GetTile((Vector3Int)pos) != tile)
                {
                    temp.Add(pos);
                }
            }
        }
        return temp;
    }

    static HashSet<Vector2Int> GetConnected(Vector2Int pos, HashSet<Vector2Int> allPos)
    {
        allPos.Remove(pos);
        HashSet<Vector2Int> result = new HashSet<Vector2Int>();
        result.Add(pos);
        foreach (Vector2Int nei in GetStraightNeighborCell(pos, allPos))
        {
            result.UnionWith(GetConnected(nei, allPos));
        }
        return result;
    }

    static bool IsConnected(HashSet<Vector2Int> allPos)
    {
        if (allPos.Count == 1) return true;

        HashSet<Vector2Int> temp = new HashSet<Vector2Int>();
        foreach (Vector2Int pos in allPos)
        {
            temp = GetConnected(pos, allPos);
            break;
        }

        if (allPos.IsProperSubsetOf(temp)) return true;
        else return false;
    }

    public static HashSet<Vector2Int> GetUndeadTilePos(HashSet<Vector2Int> tilePos)
    {
        // Get each tile element in tilePos
        // Get full neightbor
        // Check wheter the neightbor connected or not
        // If connected, then good to remove

        HashSet<Vector2Int> result = new HashSet<Vector2Int>();
        foreach (Vector2Int tile in tilePos)
        {
            if (IsConnected(GetNeighborCell(tile, tilePos))) result.Add(tile);
        }
        return result;
    }

    public static List<Vector3> GetWorldPosOfTilemapPos(Tilemap tilemap, HashSet<Vector2Int> pos, Vector3 offset = default(Vector3))
    {
        if (offset == Vector3.zero) offset = new Vector3(.5f, .5f);

        List<Vector3> result = new List<Vector3>();
        foreach (Vector2Int cellPos in pos)
        {
            result.Add(tilemap.CellToWorld((Vector3Int)cellPos) + offset);
        }
        return result;
    }

    public static int ItemCompare(Item first, Item second)
    {
        int temp = first.rarity.CompareTo(second.rarity);
        if (temp == 0) temp = first.order.CompareTo(second.order);
        if (temp == 0) temp = first.itemName.CompareTo(second.itemName);
        return temp;
    }

    public static int ItemSlotCompare(ItemSlotData first, ItemSlotData second)
    {
        int temp = first.item.rarity.CompareTo(second.item.rarity);
        if (temp == 0) temp = first.item.order.CompareTo(second.item.order);
        if (temp == 0) temp = first.item.itemName.CompareTo(second.item.itemName);
        return temp;
    }

    public static Vector3 Round(this Vector3 vector3, int decimalPlaces = 0)
    {
        float multiplier = 1;
        for (int i = 0; i < decimalPlaces; i++)
        {
            multiplier *= 10f;
        }
        return new Vector3(
            Mathf.Round(vector3.x * multiplier) / multiplier,
            Mathf.Round(vector3.y * multiplier) / multiplier,
            Mathf.Round(vector3.z * multiplier) / multiplier
        );
    }
}
