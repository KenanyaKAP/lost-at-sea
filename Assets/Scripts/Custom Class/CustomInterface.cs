using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISlotClickable
{
    void SlotClick(int id);
}
