using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TileData
{
    public uint tileId;
    public Vector2Int pos;

    public TileData(uint tileId, Vector2Int pos)
    {
        this.tileId = tileId;
        this.pos = pos;
    }

    public HashSet<Vector2Int> GetNeighborPosition()
    {
        return Utilities.GetNeighborCell(this.pos); 
    }
}

[System.Serializable]
public class ItemSlotData
{
    public Item item;
    public int count;

    public ItemSlotData(ItemSlotData itemSlotData)
    {
        item = itemSlotData.item;
        count = itemSlotData.count;
    }

    public ItemSlotData(Item item, int count)
    {
        this.item = item;
        this.count = count;
    }
}

[System.Serializable]
public class SpawnableProperties
{
    public Vector3 propsPositionOffset;
    public Vector3 propsRotationOffset;
    public Vector3 propsScaleOffset = Vector3.one;
    public bool isBoxCol = true;
    public Vector2 boxColOffset;
    public Vector2 boxColSize = Vector2.one;
    public Vector2 cirColOffset;
    public float cirColRadius = 1f;
}

[System.Serializable]
public class StorageData
{
    
}