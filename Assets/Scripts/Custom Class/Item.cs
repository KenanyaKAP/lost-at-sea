using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "New Item", menuName = "Item")]
public class Item : ScriptableObject
{
    public enum Type
    {
        None,
        Spawnable,
        Material,
        Tool
    }

    [Header("Default Properties")]
    public uint itemId;
    public string itemName = "Item Name";
    public Sprite itemSprite;
    public Type type = Type.None;
    public int rarity = 0;
    public int order = 0;

    [Header("Materials & Tools")]
    public Sprite itemSpriteOutline;
    public Vector3 materialScale = Vector3.one;
    
    [Header("Spawnable")]
    public SpawnableProperties properties;
    public PropsType propsType = PropsType.None;
    public bool isMovable = true;

    [Header("Recipe & Craft")]
    public List<ItemSlotData> recipe;
    public Item craftBy;
}
