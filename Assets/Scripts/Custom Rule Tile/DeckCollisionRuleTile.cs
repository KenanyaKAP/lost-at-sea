using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "New Deck Collision Rule Tile", menuName = "2D/Tiles/Deck Collision Rule Tile")]
public class DeckCollisionRuleTile : RuleTile<DeckCollisionRuleTile.Neighbor> {
    public TileBase baseTile;

    public class Neighbor : RuleTile.TilingRule.Neighbor {
    }

    public override bool RuleMatch(int neighbor, TileBase tile) {
        switch (neighbor) {
            case Neighbor.This: return tile == baseTile;
            case Neighbor.NotThis: return tile != baseTile;
        }
        return base.RuleMatch(neighbor, tile);
    }
}