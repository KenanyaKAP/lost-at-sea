using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Deck : MonoBehaviour
{
    public static Deck Instance;

    enum PrototypeCondition
    {
        None,
        CanAdd,
        CanRemove
    }

    [Header("Components")]
    [SerializeField] Transform propsParent;
    [SerializeField] Grid deckGrid;
    [SerializeField] TileBase baseDeck;
    [SerializeField] TileBase collisionDeck;
    [SerializeField] EditDeckInfoUI ui;

    [Header("Properties")]
    [SerializeField] int woodCountToAdd = 5;

    [Header("Adder Elements")]
    [SerializeField] GameObject popUpPrefabs;
    [SerializeField] Sprite baseDeckSprite;
    [SerializeField] Sprite cantRemoveSprite;
    
    [Header("Prototype")]
    [SerializeField] SpriteRenderer prototype;

    [Header("Tilemaps")]
    [SerializeField] Tilemap baseDeckTilemap;
    public Tilemap GetBaseDeckTilemap() => baseDeckTilemap;

    [Header("Attributes")]
    [SerializeField] List<Vector2Int> exclusiveNotToRemoveDeckCell = new List<Vector2Int>();
    HashSet<Vector2Int> baseDeckCell = new HashSet<Vector2Int>();
    HashSet<Vector2Int> outerBaseDeckCell = new HashSet<Vector2Int>();
    HashSet<Vector2Int> canAddBaseDeckCell = new HashSet<Vector2Int>();
    HashSet<Vector2Int> canRemoveBaseDeckCell = new HashSet<Vector2Int>();
    HashSet<Vector2Int> freeBaseDeckCell = new HashSet<Vector2Int>();
    PrototypeCondition prototypeCondition;

    bool isEditDeck;

    void Awake()
    {
        if (!Instance) Instance = this;
        else Destroy(this);
    }

    void Start()
    {
        UpdateTiling();
        UpdatePrototypeVisual();
        ui.SetCount(PlayerInventory.Instance.GetSpecifiedItemCount(0), woodCountToAdd);
    }

    void Update()
    {
        if (isEditDeck)
        {
            MovePrototype(Locator.Instance.transform.position.Round());
        }
    }

    public void UpdateTiling()
    {
        // Get Base & Outer Tile
        baseDeckCell = Utilities.GetTilePosOfType(baseDeckTilemap, baseDeck);
        outerBaseDeckCell = Utilities.GetOuterTilePosOfType(baseDeckTilemap, baseDeck, baseDeckCell);
        
        // Fill Outer Tile with Collision Tile
        foreach (Vector2Int pos in outerBaseDeckCell)
        {
            baseDeckTilemap.SetTile((Vector3Int)pos, collisionDeck);
        }

        // Add deck Pos that is free of props
        freeBaseDeckCell = new HashSet<Vector2Int>(baseDeckCell);
        for (int i = 0; i < propsParent.childCount; i++)
        {
            freeBaseDeckCell.Remove((Vector2Int)baseDeckTilemap.WorldToCell(propsParent.GetChild(i).position));
        }
        foreach (Vector2Int cell in exclusiveNotToRemoveDeckCell)
        {
            freeBaseDeckCell.Remove(cell);
        }

        // Add deck Pos that can be added to the deck. which is outer straight from free deck
        canAddBaseDeckCell = Utilities.GetOuterStraightTilePosOfType(baseDeckTilemap, baseDeck, freeBaseDeckCell);

        // Free deck is also can be remove
        canRemoveBaseDeckCell = new HashSet<Vector2Int>(freeBaseDeckCell);
        
        // Finaly, get the Undead of Deck that can be remove
        canRemoveBaseDeckCell = Utilities.GetUndeadTilePos(canRemoveBaseDeckCell);
    }

    public void OnClick()
    {
        Vector3Int tempCell = baseDeckTilemap.WorldToCell(prototype.transform.position);
        
        if (prototypeCondition == PrototypeCondition.CanAdd)
        {
            if (PlayerInventory.Instance.GetSpecifiedItemCount(0) >= woodCountToAdd)
            {
                baseDeckTilemap.SetTile(tempCell, baseDeck);
                baseDeckCell.Add((Vector2Int)tempCell);
                PlayerInventory.Instance.RemoveItem(0, woodCountToAdd);

                ItemPopUp temp = Instantiate(popUpPrefabs, Locator.Instance.transform.position, Quaternion.identity).GetComponent<ItemPopUp>();
                temp.StartPopUp(GameInstance.Instance.GetGameItems().Find((x) => x.itemId == 0), -woodCountToAdd);
            }
            else
            {
                ui.ShakeUI();
            }
        }
        else if (prototypeCondition == PrototypeCondition.CanRemove)
        {
            baseDeckTilemap.SetTile((Vector3Int)tempCell, null);
            foreach (Vector2Int cell in Utilities.GetTilePosOfType(baseDeckTilemap, collisionDeck))
            {
                baseDeckTilemap.SetTile((Vector3Int)cell, null);
            }
            baseDeckCell.Remove((Vector2Int)tempCell);
            
            ItemPopUp temp = Instantiate(popUpPrefabs, Locator.Instance.transform.position, Quaternion.identity).GetComponent<ItemPopUp>();
            temp.StartPopUp(GameInstance.Instance.GetGameItems().Find((x) => x.itemId == 0), woodCountToAdd);

            int returnCount = PlayerInventory.Instance.CanAddItemWithReturn(0, 5);
            if (returnCount != 0) PlayerInventory.Instance.SpawnItem(0, returnCount);
        }

        UpdateTiling();
        UpdatePrototypeVisual();
        ui.SetCount(PlayerInventory.Instance.GetSpecifiedItemCount(0), woodCountToAdd);
    }

    public void StartEdit()
    {
        Player.Instance.ChangeState(Player.State.EditDeck);
        isEditDeck = true;

        ui.gameObject.SetActive(true);
        ui.SetCount(PlayerInventory.Instance.GetSpecifiedItemCount(0));
        prototype.gameObject.SetActive(true);
    }

    public void StopEdit()
    {
        Player.Instance.ChangeState(Player.State.Default);
        isEditDeck = false;

        ui.gameObject.SetActive(false);
        prototype.gameObject.SetActive(false);
    }

    void MovePrototype(Vector3 pos)
    {
        if (prototype.transform.position == pos) return;
        prototype.transform.position = pos;        

        UpdatePrototypeVisual();
    }

    void UpdatePrototypeVisual()
    {
        if (canAddBaseDeckCell.Contains((Vector2Int)baseDeckTilemap.WorldToCell(prototype.transform.position))) prototypeCondition = PrototypeCondition.CanAdd;
        else if (canRemoveBaseDeckCell.Contains((Vector2Int)baseDeckTilemap.WorldToCell(prototype.transform.position))) prototypeCondition = PrototypeCondition.CanRemove;
        else prototypeCondition = PrototypeCondition.None;

        switch (prototypeCondition)
        {
            case PrototypeCondition.None:
                prototype.sprite = cantRemoveSprite;
                prototype.color = new Color(1, 0, 0, .5f);
                break;
            case PrototypeCondition.CanAdd:
                prototype.sprite = baseDeckSprite;
                prototype.color = new Color(0, 1, 0, .5f);
                break;
            case PrototypeCondition.CanRemove:
                prototype.sprite = baseDeckSprite;
                prototype.color = new Color(1, 0, 0, .5f);
                break;
        }
    }

    public HashSet<Vector2Int> GetFreeDeckCell()
    {
        UpdateFreeDeckCell();
        
        return freeBaseDeckCell;
    }

    public void UpdateFreeDeckCell()
    {
        // Update
        // Add deck Pos that is free of props
        freeBaseDeckCell = new HashSet<Vector2Int>(baseDeckCell);
        for (int i = 0; i < propsParent.childCount; i++)
        {
            freeBaseDeckCell.Remove((Vector2Int)baseDeckTilemap.WorldToCell(propsParent.GetChild(i).position));
        }
        foreach (Vector2Int cell in exclusiveNotToRemoveDeckCell)
        {
            freeBaseDeckCell.Remove(cell);
        }
    }
}
