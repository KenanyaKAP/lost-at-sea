using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageUI : MonoBehaviour, ISlotClickable
{
    public static StorageUI Instance;

    [Header("Properties")]
    [SerializeField] PropsStorage curStorage;

    [Header("Components")]
    [SerializeField] GameObject storageBackButton;
    [SerializeField] Transform storageSlotParent;
    [SerializeField] Transform inventorySlotParent;
    [SerializeField] GameObject inventorySlotPrefabs;

    List<InventorySlot> storageSlot = new List<InventorySlot>();
    List<InventorySlot> inventorySlot = new List<InventorySlot>();

    void Awake()
    {
        if (!Instance) Instance = this;
        else Destroy(this);

        gameObject.SetActive(false);
        storageBackButton.SetActive(false);
    }

    public void OpenStorage(PropsStorage storage)
    {
        curStorage = storage;

        // Activating UI
        gameObject.SetActive(true);
        storageBackButton.SetActive(true);

        // Setting the data
        SetChestUI(curStorage.GetItemsBySlot(), curStorage.GetStorageSize());
        SetInventoryUI(PlayerInventory.Instance.GetItemsBySlot());
    }

    void UpdateChestItem(List<ItemSlotData> slotData)
    {
        // Setting the item in slot
        foreach (InventorySlot slot in storageSlot) slot.ClearInventorySlot();
        for (int i = 0; i < slotData.Count; i++)
        {
            storageSlot[i].SetItem(slotData[i]);
        }
    }

    void UpdateInventoryItem(List<ItemSlotData> slotData)
    {
        // Setting the item in slot
        foreach (InventorySlot slot in inventorySlot) slot.ClearInventorySlot();
        for (int i = 0; i < slotData.Count; i++)
        {
            inventorySlot[i].SetItem(slotData[i]);
        }
    }

    void SetChestUI(List<ItemSlotData> slotData, int chestSize)
    {
        // Create the Slot UI
        storageSlot.Clear();
        for (int i = 0; i < storageSlotParent.childCount; i++)
        {
            Destroy(storageSlotParent.GetChild(i).gameObject);
        }
        for (int i = 0; i < chestSize; i++)
        {
            InventorySlot newSlot = Instantiate(inventorySlotPrefabs, storageSlotParent).GetComponent<InventorySlot>();
            newSlot.InitializeInventorySlot(this, 100 + i);
            storageSlot.Add(newSlot);
        }

        UpdateChestItem(slotData);
    }

    public void SetInventoryUI(List<ItemSlotData> slotData)
    {
        // Create the Slot UI
        inventorySlot.Clear();
        for (int i = 0; i < inventorySlotParent.childCount; i++)
        {
            Destroy(inventorySlotParent.GetChild(i).gameObject);
        }
        for (int i = 0; i < PlayerInventory.Instance.GetMaxSlot(); i++)
        {
            InventorySlot newSlot = Instantiate(inventorySlotPrefabs, inventorySlotParent).GetComponent<InventorySlot>();
            newSlot.InitializeInventorySlot(this, i);
            inventorySlot.Add(newSlot);
        }

        UpdateInventoryItem(slotData);
    }

    public void SlotClick(int id)
    {
        if (id < 100) // For Inventory
        {        
            ItemSlotData temp = inventorySlot[id].GetItem();
            if (temp != null)
            {
                if (curStorage.CanAddItemWithReturn(temp))
                {
                    PlayerInventory.Instance.RemoveItem(temp);
                    UpdateChestItem(curStorage.GetItemsBySlot());
                    UpdateInventoryItem(PlayerInventory.Instance.GetItemsBySlot());
                }
            }
        }
        else // For Storage
        {
            ItemSlotData temp = storageSlot[id - 100].GetItem();
            if (temp != null)
            {
                if (PlayerInventory.Instance.CanAddItemWithReturn(temp))
                {
                    curStorage.RemoveItem(temp);
                    UpdateChestItem(curStorage.GetItemsBySlot());
                    UpdateInventoryItem(PlayerInventory.Instance.GetItemsBySlot());
                }
            }
        }
    }

    public void MoveProps()
    {
        curStorage.MoveProps();
    }

    public void DestroyProps()
    {
        curStorage.DestroyProps();
    }
}
