using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkbenchBuildPropsMaterialUpdater : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] TMPro.TextMeshProUGUI text;

    [Header("Attributes")]
    [SerializeField] int itemId;
    [SerializeField] int itemMinimalCount;

    void OnEnable()
    {
        text.text = "(" + PlayerInventory.Instance.GetSpecifiedItemCount(itemId).ToString() + ")";
        if (PlayerInventory.Instance.GetSpecifiedItemCount(itemId) < itemMinimalCount)
        {
            text.color = new Color(1, 0.2877358f, 0.2877358f, 1);
        }
        else
        {
            text.color = Color.white;
        }
    }
}
