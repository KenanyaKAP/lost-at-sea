using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditDeckInfoUI : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] TMPro.TextMeshProUGUI text;
    [SerializeField] RectTransform woodImage;

    Vector3 textPos;
    Vector3 woodImagePos;
    void Awake()
    {
        textPos = text.transform.position;
        woodImagePos = woodImage.transform.position;
    }

    public void SetCount(int count, int minCount = 5)
    {
        text.text = count.ToString();
        if (count < minCount)
        {
            text.color = new Color(1, .35f, .35f, 1);
        }
        else text.color = Color.white;
    }

    public void ShakeUI()
    {
        text.transform.position = textPos;
        woodImage.transform.position = woodImagePos;
        LeanTween.cancel(text.gameObject);
        LeanTween.cancel(woodImage.gameObject);
        LeanTween.move(text.gameObject, textPos + Vector3.right * 5, .1f).setEaseShake().setLoopCount(5);
        LeanTween.move(woodImage.gameObject, woodImagePos + Vector3.right * 5, .1f).setEaseShake().setLoopCount(5);
    }
}
