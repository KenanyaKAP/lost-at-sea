using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class InventorySlot : MonoBehaviour
{
    [Header("Component")]
    [SerializeField] GameObject iSlotClickableClass;
    [SerializeField] Image image;
    [SerializeField] TMPro.TextMeshProUGUI text;
    
    [Header("Properties")]
    [SerializeField] int inventorySlotId = 0;
    [SerializeField] Sprite noneSpite;

    [Header("Item")]
    [SerializeField] ItemSlotData item;
    public ItemSlotData GetItem() => item;

    ISlotClickable clickableUI;

    void Awake()
    {
        if (iSlotClickableClass)
        {
            clickableUI = iSlotClickableClass.GetComponent(typeof(ISlotClickable)) as ISlotClickable;
        }
    }

    void Start() => SetItem(item);

    public void InitializeInventorySlot(ISlotClickable parent, int id)
    {
        clickableUI = parent;
        inventorySlotId = id;
    }

    public void SetItem(ItemSlotData newItem)
    {
        if (newItem == null || !newItem.item)
        {
            ClearInventorySlot();
            return;
        }

        item = newItem;

        image.sprite = item.item.itemSprite;
        if (item.count > 1) text.text = item.count.ToString();
        else text.text = "";
    }

    public void ClearInventorySlot()
    {
        item = null;
        image.sprite = noneSpite;
        text.text = "";
    }

    public void InventoryClick()
    {
        clickableUI.SlotClick(inventorySlotId);
    }
}
