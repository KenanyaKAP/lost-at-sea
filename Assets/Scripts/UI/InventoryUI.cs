using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour, ISlotClickable
{
    [Header("Internal Component")]
    [SerializeField] Transform itemSlotParent;
    [SerializeField] GameObject inventorySlotPrefabs;
    
    List<InventorySlot> inventorySlot = new List<InventorySlot>();

    void OnEnable()
    {
        SetInventoryUI(PlayerInventory.Instance.GetItemsBySlot());
    }

    public void SetInventoryUI(List<ItemSlotData> slotData)
    {
        inventorySlot.Clear();
        for (int i = 0; i < itemSlotParent.childCount; i++)
        {
            Destroy(itemSlotParent.GetChild(i).gameObject);
        }
        for (int i = 0; i < PlayerInventory.Instance.GetMaxSlot(); i++)
        {
            InventorySlot newSlot = Instantiate(inventorySlotPrefabs, itemSlotParent).GetComponent<InventorySlot>();
            newSlot.InitializeInventorySlot(this, i);
            inventorySlot.Add(newSlot);
        }

        foreach (InventorySlot slot in inventorySlot) slot.ClearInventorySlot();
        for (int i = 0; i < slotData.Count; i++)
        {
            inventorySlot[i].SetItem(slotData[i]);
        }
    }

    public void SlotClick(int slotId)
    {
        ItemSlotData temp = inventorySlot[slotId].GetItem();
        if (temp != null)
        {
            PlayerInventory.Instance.RemoveItem(temp);
            PlayerInventory.Instance.SpawnItem(temp);
            SetInventoryUI(PlayerInventory.Instance.GetItemsBySlot());
        }
    }
}
