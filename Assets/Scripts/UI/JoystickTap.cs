using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickTap : MonoBehaviour
{
    public delegate void OnJoystickTap();
    public static OnJoystickTap onJoystickTap;

    public void Tap()
    {
        onJoystickTap?.Invoke();
    }
}
